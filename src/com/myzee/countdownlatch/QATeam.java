package com.myzee.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class QATeam extends Thread {
	
	CountDownLatch latch;
	public QATeam(CountDownLatch latch, String name) {
		super(name);
		this.latch = latch;
	}
	
	@Override
	public void run() {
		try {
			System.out.println("task assigned to " + Thread.currentThread().getName());
			Thread.sleep(1000);
			latch.countDown();
			System.out.println("task finished by " + Thread.currentThread().getName());
		} catch (Exception e) {
		}
	}
	
}
