package com.myzee.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class ProjectStatus {

	public static void main(String[] args) {
		
		CountDownLatch latch = new CountDownLatch(2);
		DevTeam teamA = new DevTeam(latch, "Dev A");
		DevTeam teamB  =new DevTeam(latch, "Dev B");
		
		teamA.start();
		teamB.start();
		
		try {
			latch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("---------------Status-----------");
		QATeam qateam = new QATeam(latch, "QA Team");
		qateam.start();
		
	}

}
