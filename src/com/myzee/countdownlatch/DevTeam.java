package com.myzee.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class DevTeam extends Thread {
	
	CountDownLatch latch;
	public DevTeam(CountDownLatch latch, String name) {
		super(name);
		this.latch = latch;
		
	}
	
	@Override
	public void run() {
		try {
			System.out.println("task assigned to " + Thread.currentThread().getName());
			Thread.sleep(1000);
			System.out.println("task completed by " + Thread.currentThread().getName());
			latch.countDown();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
}
