package com.myzee.cyclicBarrier.passangerApp;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class PassangerThread extends Thread {
	
	private int timeToWait;
	private String passName;
	private CyclicBarrier barrier;
	
	public PassangerThread(int durationToWait, CyclicBarrier barrier,  String passName) {
		super(passName);
		this.timeToWait = durationToWait;
		this.barrier = barrier;
	}
	@Override
	public void run() {
		
		try {
			Thread.sleep(timeToWait);
			System.out.println(Thread.currentThread().getName() + " has arrived the location!");
			int await = barrier.await();
//			System.out.println(await);
			if(await == 0)
				System.out.println("Four passangers arrived, Cab will start soon!!");
		} catch (InterruptedException | BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
