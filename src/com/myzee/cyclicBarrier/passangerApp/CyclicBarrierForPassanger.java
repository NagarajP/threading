package com.myzee.cyclicBarrier.passangerApp;

import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierForPassanger {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CyclicBarrier barrier = new CyclicBarrier(4);
		
		System.out.println("main thread started!");
		
		PassangerThread p1 = new PassangerThread(1000, barrier, "Jhon");
		PassangerThread p2 = new PassangerThread(2000, barrier, "Brett");
		PassangerThread p3 = new PassangerThread(3000, barrier, "Khaty");
		PassangerThread p4 = new PassangerThread(4000, barrier, "Pyter");
		
		PassangerThread p5 = new PassangerThread(1000, barrier, "Bolton");
		PassangerThread p6 = new PassangerThread(2000, barrier, "Joy");
		PassangerThread p7 = new PassangerThread(3000, barrier, "Drogo");
		PassangerThread p8 = new PassangerThread(4000, barrier, "Killer");
		
		p1.start();
		p2.start();
		p3.start();
		p4.start();
		
		p5.start();
		p6.start();
		p7.start();
		p8.start();
		
		System.out.println("main thread finished!");
	}

}
